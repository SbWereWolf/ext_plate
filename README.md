Sitepackage for the project "ext plate"
=
# How To Install

## Add Typo Script
For root page :
```$xslt
lib.logout = CONTENT
lib.logout {
    table = tt_content
    select.uidInList = 5 # change with proper value
    select.pidInList = 5 # change with proper value
}

plugin.tx_felogin_pi1.redirectPageLogout = 3 # change with proper value
plugin.tx_felogin_pi1.redirectPageLogin = 6 # change with proper value
```
For login page :
```$xslt
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:ext_plate/Configuration/TypoScript/login-page.typoscript">
```
For news page :
```$xslt
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:ext_plate/Configuration/TypoScript/news-page.typoscript">
```
