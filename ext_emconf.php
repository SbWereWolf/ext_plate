<?php

/**
 * Extension Manager/Repository config file for ext "ext_plate".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'ext plate',
    'description' => '',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-9.5.99',
            'fluid_styled_content' => '9.5.0-9.5.99',
            'rte_ckeditor' => '9.5.0-9.5.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Topliner\\ExtPlate\\' => 'Classes',
        ],
    ],
    'state' => 'alpha',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Volkhin Nikolay',
    'author_email' => 'nv@topliner.ru',
    'author_company' => 'topliner',
    'version' => '1.0.0',
];
