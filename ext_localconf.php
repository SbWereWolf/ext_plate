<?php
/**
 * Copyright © 2020
 * Volkhin Nikolay <ulfnew@gmail.com> price-activity
 * 15.2.2020 1:37
 */

defined('TYPO3_MODE') || die();

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['ext_plate'] = 'EXT:ext_plate/Configuration/RTE/Default.yaml';

/***************
 * PageTS
 */
/** @noinspection PhpFullyQualifiedNameUsageInspection */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:'
    . $_EXTKEY
    . '/Configuration/TsConfig/Page/All.tsconfig">'
);
